# Service Oriented Architecture (SOA):

## Introduction:
---
   
Service-Oriented Architecture (SOA) is a style of software design where services are provided to the other components by application components through a communication protocol over a network.

## Characteristics Of Service-Oriented Architecture:
___
 Service Oriented Architecture has six major characterstics, let us have  a look at those charaterstics

     1.Business value
     2.Strategic goals
     3.Intrinsic inter-operability
     4.Shared services
     5.Flexibility
     6.Evolutionary refinement.

## SOA ARCHITECTURE:
---
![IMAGE](https://miro.medium.com/max/1400/1*Xot9nbkQAGbGaYwi84Kh-w.png)

## Block Diagram of SOA Architecture WORKING:
___

 ![IMAGE](https://static.javatpoint.com/webservicepages/images/soa2.png)


* Services - The services are the logical entities defined by one or more published interfaces.

* Service provider - It is a software entity that implements a service specification.

* Service consumer - It can be called as a requestor or client that calls a service provider. A service consumer can be another service or an end-user application.

* Service locator - It is a service provider that acts as a registry. It is responsible for examining service provider interfaces and service locations.

* Service broker - It is a service provider that pass service requests to one or more additional service providers.
 
 ## Benifits of SOA:
 --- 
    Compared to the architectures that preceded it, SOA offered significant benefits to the enterprise:

* ## Greater business agility: 
    faster time to market: Reusability is key.  The efficiency of assembling applications from reusable services - i.e. building blocks, rather than rewriting and reintegrating with every new development project, enables developers to build applications much more quickly in response to new business opportunities. The service oriented architectural approach supports scenarios for application integration, data integration, and service orchestration style automation of business processes or workflows.  This speeds software design and software development by enabling developers to spend dramatically less time integrating and much more time focusing on delivering and improving their applications. 

* ## Ability to leverage legacy functionality in new markets:
    company’s employees or business partners.
    Improved collaboration between business and IT: In an SOA, services can be defined in business terms (e.g., ‘generate insurance quote’ or ‘calculate capital equipment ROI’). This enables business analysts to work more effectively with developers on important insights—such as the scope of a business process defined using services or the business implications of changing a process—that can lead to a better result.

# SOA Examples
By 2010, SOA implementations were going full steam at leading companies in virtually every industry. For example:

* Delaware Electric turned to SOA to integrate systems that   previously did not talk to each other, resulting in   development efficiencies that helped the organization stay solvent during a five-year, state-mandated freeze on electric rates.
* Cisco adopted SOA to make sure its product ordering experience was consistent across all products and channels by exposing ordering processes as services that Cisco’s divisions, acquisitions, and business partners could incorporate into their websites.

## Conclusion:
---
 As we have optimal  scalability and reliabity experience by using SOA architecture, we must use it in our project.


## Refernces:
___
https://en.wikipedia.org/wiki/Service-oriented_architecture

https://www.geeksforgeeks.org/service-oriented-architecture

https://medium.com/@SoftwareDevelopmentCommunity/what-is-service-oriented-architecture-fa894d11a7ec

https://www.ibm.com/cloud/learn/soa#toc-benefits-o-YQlLJ50n



